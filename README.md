CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Maintainers


INTRODUCTION
------------

This module extends the Drupal core CSRF token validation with support for anonymous users and single use CSRF tokens.

REQUIREMENTS
------------

Drupal core `8.8.3` or up.

INSTALLATION
------------

 * Install the Anonymous CSRF Token module as you would normally install a contributed
   Drupal module. Visit [Installing Drupal 8 Modules](https://www.drupal.org/node/1897420) for further
   information.

USAGE
-----

### 8.x-1.x

See [CSRF Anonymous Token](https://www.drupal.org/project/csrf_anonymous_token) for the Drupal 8 variant of this module's `7.x-1.x` branch.

### 2.x

The `2.x` release of this module is completely different in design, implementation, and purpose. Version `2.0+` of this module does not wire up anonymous CSRF protection automatically. Rather, you must explicitly wire up each route you wish to protect `*.routing.yml` and enable CSRF token protection for each route via specifying `_anonymous_csrf_token: 'TRUE'` in the route's declaration.

You will also need to call this module's `AnonymousCsrfTokenGenerator` service that wraps the `CsrfTokenGenerator` from core in order to complete the implementation:

```
/** @var \Drupal\anonymous_token\Access\AnonymousCsrfTokenGenerator $csrf_token */
$csrf_token = \Drupal::service('anonymous_token.csrf_token');
```

MAINTAINERS
-----------

 * Nikolay Yenbakhtov (NikolayYenbakhtov) - https://www.drupal.org/u/nikolayyenbakhtov
 * Sang Lostrie (baikho) - https://www.drupal.org/u/baikho

### Supporting organizations:

 * [Access](https://www.drupal.org/access) - Development and maintenance
