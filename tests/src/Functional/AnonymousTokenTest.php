<?php

namespace Drupal\Tests\anonymous_token\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests anonymous_token module.
 *
 * @group anonymous_token
 */
class AnonymousTokenTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'anonymous_token_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test hitting the test route.
   */
  public function testAnonymousToken() {
    /* @var \Drupal\anonymous_token\Access\AnonymousCsrfTokenGenerator $anonymous_token_service */
    $anonymous_token_service = \Drupal::service('anonymous_token.csrf_token');

    // Get a token and then immediately validate.
    $token = $anonymous_token_service->get();
    $this->assertTrue($anonymous_token_service->validate($token));

    // Get a token from a special test route. If we get the token
    // using the anonymous_token.csrf_token service directly, the
    // token is considered invalid on the test route. This is likely
    // due to anonymous sessions being a little funky in tests.
    $this->drupalGet('/anonymous-token-test/token');
    $token = $this->getSession()->getPage()->getContent();

    // Use the token on the protected route.
    $path = '/anonymous-token-test/test';
    $options = [
      'query' => [
        'token' => $token,
      ],
    ];
    $this->drupalGet($path, $options);
    $this->assertSession()->statusCodeEquals(200);

    // Try without the token.
    $this->drupalGet($path);
    $this->assertSession()->statusCodeEquals(403);

    // Change settings to force_single_use of token.
    $settings = \Drupal::configFactory()->getEditable('anonymous_token.settings');
    $settings->set('force_single_use', TRUE)->save();

    // The token should work one more time and then fail.
    $this->drupalGet($path, $options);
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet($path, $options);
    $this->assertSession()->statusCodeEquals(403);
  }

}
