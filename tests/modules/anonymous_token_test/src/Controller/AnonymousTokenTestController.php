<?php

namespace Drupal\anonymous_token_test\Controller;

use Drupal\Core\Access\CsrfRequestHeaderAccessCheck;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller to support testing anonymous_token module.
 */
class AnonymousTokenTestController extends ControllerBase {

  /**
   * Returns a simple response for testing.
   *
   * @return Symfony\Component\HttpFoundation\Response
   */
  public function build() {
    return new Response('Hello world');
  }

  /**
   * Returns a token.
   *
   * @return Symfony\Component\HttpFoundation\Response
   */
  public function token() {
    $token = \Drupal::service('anonymous_token.csrf_token')->get('anonymous-token-test/test');
    return new Response($token);
  }

}
