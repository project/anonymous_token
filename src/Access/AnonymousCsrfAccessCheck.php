<?php

namespace Drupal\anonymous_token\Access;

use Drupal\Core\Access\CsrfAccessCheck;

/**
 * Class AnonymousCsrfAccessCheck.
 *
 * @package Drupal\anonymous_token\Access
 */
class AnonymousCsrfAccessCheck extends CsrfAccessCheck {

  /**
   * The CSRF token generator.
   *
   * @var \Drupal\anonymous_token\Access\AnonymousCsrfTokenGenerator
   */
  protected $csrfToken;

  /**
   * Constructs a AnonymousCsrfAccessCheck object.
   *
   * @param \Drupal\anonymous_token\Access\AnonymousCsrfTokenGenerator $csrf_token
   *   The CSRF token generator.
   */
  public function __construct(AnonymousCsrfTokenGenerator $csrf_token) {
    // We can't pass it to the parent as it's annotated with a class and not an
    // interface so override it here.
    $this->csrfToken = $csrf_token;
  }

}
