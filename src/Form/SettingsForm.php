<?php

namespace Drupal\anonymous_token\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\anonymous_token\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'anonymous_token_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['anonymous_token.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('anonymous_token.settings');

    $form['force_single_use'] = [
      '#title' => $this->t('Force single use tokens'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('force_single_use') ?? FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('anonymous_token.settings')
      ->set('force_single_use', (bool) $form_state->getValue('force_single_use'))
      ->save();
  }

}
